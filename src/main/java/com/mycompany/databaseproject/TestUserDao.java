/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import com.mycompany.databaseproject.dao.userDao;
import com.mycompany.databaseproject.helper.databaseHelper;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author WIN10
 */
public class TestUserDao {
    public static void main(String[] args) {
        userDao userDao = new userDao();
        for(User u: userDao.getAll()){
            System.out.println(u);
        }
        
        User user1 = userDao.get(2);
        System.out.println(user1);
        
//        User newUser = new User("user3","password",2,"F");
//        User insertedUser = userDao.save(newUser);
//        System.out.println(insertedUser);
        user1.setGender("F");
        userDao.update(user1);
        User updateUser = userDao.get(user1.getId());
        System.out.println(updateUser);
        
        userDao.delete(user1);
        for(User u: userDao.getAll()){
            System.out.println(u);
        }


        databaseHelper.close();
    }

}
