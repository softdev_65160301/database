/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author WIN10
 */
public class Delete {
    public static void main(String[] args) {
        //connect
        Connection con = null;
        String url = "jdbc:sqlite:dcoffee.db";
        try {
            con = DriverManager.getConnection(url);
            System.out.println("Connected");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        //Delete
        String sql = "DELETE FROM category WHERE cat_id=?";
        try {
            PreparedStatement stmt = con.prepareStatement(sql);
            stmt.setInt(1, 9);
            int status = stmt.executeUpdate();
            //ResultSet key =  stmt.getGeneratedKeys();
            //key.next();
            //System.out.println(""+key.getInt(1));
            
        } catch (SQLException ex) {
             System.out.println(ex.getMessage());
        }
        //close
        if (con != null) {
            try {
                con.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

}
