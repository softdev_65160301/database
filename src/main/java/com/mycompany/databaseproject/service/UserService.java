/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject.service;

import com.mycompany.databaseproject.dao.userDao;
import com.mycompany.databaseproject.model.User;

/**
 *
 * @author WIN10
 */
public class UserService {
    public User login(String name,String password) {
        userDao userDao = new userDao();
        User user = userDao.getByName(name);
        if(user!=null && user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
